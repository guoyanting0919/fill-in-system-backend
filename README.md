## Fill-In-System V1.0.0

### Step1

```
git clone https://gitlab.com/guoyanting0919/fill-in-system-backend.git
```

### Step2

```
cd fill-in-system-backend
```

### Step3

```
git checkout dev
```

### Step4

```
npm i
npm run prepare
```

### Step5

```
npm run serve
```

### Compiles and minifies for production

### 將自動產出 src/view 裡面的 file 丟到 repo **src/views/** 下

![img1](./READMEIMG/image1.png 'markdown')

### 將自動產出 src/api 裡面的 file 丟到 repo **src/api/allApi** 下

![img2](./READMEIMG/image2.png 'markdown')

### 到 src/api/index import 剛加入的檔案

![img3](./READMEIMG/image3.png 'markdown')
