const path = require('path')
const { gitDescribe, gitDescribeSync } = require('git-describe')
process.env.VUE_APP_GIT_HASH = gitDescribeSync().hash

function resolve(dir) {
  return path.join(__dirname, '/', dir)
}

module.exports = {
  // publicPath: "/backStage/",
  lintOnSave: process.env.NODE_ENV !== 'production',
  devServer: {
    port: 1804, // 端口
    overlay: {
      warnings: true,
      errors: false,
    },
  },
  runtimeCompiler: true,
  configureWebpack: {
    devtool: 'source-map',
  },
  productionSourceMap: false,
}
