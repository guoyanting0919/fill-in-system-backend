# 建立 node.js base build stage
FROM node:14.15.4 as build-stage 
WORKDIR /app
COPY package*.json ./
COPY nginx.conf ./
RUN npm install
COPY . .
ARG NODE_ENV
RUN npm run build:${NODE_ENV}

# production stage
FROM nginx:stable-alpine as production-stage
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build-stage /app/dist .
COPY --from=build-stage /app/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]