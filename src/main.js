import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/custom-theme/index.css'
import locale from 'element-ui/lib/locale/lang/zh-TW'
import VueContextMenu from 'vue-contextmenu'
import Swal from 'sweetalert2'

import '@/styles/index.scss'

import App from './App'
import router from './router'
import store from './store'

import api from '@/api/index'
import '@/permission'
import { VueReCaptcha } from 'vue-recaptcha-v3'

import '@/assets/public/css/comIconfont/iconfont.css'
import '@/assets/public/css/comIconfont/iconfont.js'

//工作流使用的圖標
import '@/assets/public/css/workflowicon/iconfont.css'
import '@/assets/public/css/workflowicon/iconfont.js'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
import quillEditor from 'vue-quill-editor'
Vue.use(quillEditor)

Vue.prototype.$api = api
Vue.use(ElementUI, { locale })
Vue.use(VueContextMenu)
Vue.use(VueReCaptcha, {
  siteKey: process.env.VUE_APP_SITEKEY,
})
Vue.prototype.$swal = Swal

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: (h) => h(App),
})
