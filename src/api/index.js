import accessobjs from './allApi/accessObjs'
import applications from './allApi/applications'
import buildertables from './allApi/builderTables'
import categorys from './allApi/categorys'
import dataprivilegerules from './allApi/dataprivilegerules'
import flowinstances from './allApi/flowinstances'
import flowschemes from './allApi/flowschemes'
import forms from './allApi/forms'
import login from './allApi/login'
import modules from './allApi/modules'
import orgs from './allApi/orgs'
import resources from './allApi/resources'
import roles from './allApi/roles'
import users from './allApi/users'
import syslogs from './allApi/syslogs'
import sysmessages from './allApi/sysmessages'
import hotNews from './allApi/hotNews'
import webContents from './allApi/webContents'
import lazyBagLinks from './allApi/lazyBagLinks'
import relatedLinks from './allApi/relatedLinks'

export default {
  relatedLinks,
  lazyBagLinks,
  accessobjs,
  applications,
  buildertables,
  categorys,
  dataprivilegerules,
  flowinstances,
  flowschemes,
  forms,
  login,
  modules,
  orgs,
  resources,
  roles,
  users,
  syslogs,
  sysmessages,
  hotNews,
  webContents,
}
