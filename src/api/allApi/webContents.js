import request from '@/utils/request'

const api = {
  getList(params) {
    return request({
      url: '/webContents/load',
      method: 'get',
      params,
    })
  },
  get(params) {
    return request({
      url: '/webContents/get',
      method: 'get',
      params,
    })
  },
  add(data) {
    return request({
      url: '/webContents/add',
      method: 'post',
      data,
    })
  },
  update(data) {
    return request({
      url: '/webContents/update',
      method: 'post',
      data,
    })
  },
  del(data) {
    return request({
      url: '/webContents/delete',
      method: 'post',
      data,
    })
  },
}

export default api
