import request from '@/utils/request'

const api = {
  getList(params) {
    return request({
      url: '/hotNews/load',
      method: 'get',
      params,
    })
  },
  get(params) {
    return request({
      url: '/hotNews/get',
      method: 'get',
      params,
    })
  },
  add(data) {
    return request({
      url: '/hotNews/add',
      method: 'post',
      data,
    })
  },
  update(data) {
    return request({
      url: '/hotNews/update',
      method: 'post',
      data,
    })
  },
  del(data) {
    return request({
      url: '/hotNews/delete',
      method: 'post',
      data,
    })
  },
}

export default api
