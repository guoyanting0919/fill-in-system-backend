import request from '@/utils/request'

const api = {
  getList(params) {
    return request({
      url: '/lazyBagLinks/load',
      method: 'get',
      params,
    })
  },
  get(params) {
    return request({
      url: '/lazyBagLinks/get',
      method: 'get',
      params,
    })
  },
  add(data) {
    return request({
      url: '/lazyBagLinks/add',
      method: 'post',
      data,
    })
  },
  update(data) {
    return request({
      url: '/lazyBagLinks/update',
      method: 'post',
      data,
    })
  },
  del(data) {
    return request({
      url: '/lazyBagLinks/delete',
      method: 'post',
      data,
    })
  },
}

export default api
