import request from '@/utils/request'

const api = {
  getList(params) {
    return request({
      url: '/relatedLinks/load',
      method: 'get',
      params,
    })
  },
  get(params) {
    return request({
      url: '/relatedLinks/get',
      method: 'get',
      params,
    })
  },
  add(data) {
    return request({
      url: '/relatedLinks/add',
      method: 'post',
      data,
    })
  },
  update(data) {
    return request({
      url: '/relatedLinks/update',
      method: 'post',
      data,
    })
  },
  del(data) {
    return request({
      url: '/relatedLinks/delete',
      method: 'post',
      data,
    })
  },
}

export default api
